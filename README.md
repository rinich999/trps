## About GitLab Flow
### Production branch with GitLab flow
You can make a production branch that reflects the deployed code. You can deploy a new version by merging master into the production branch. If you need to know what code is in production, you can just checkout the production branch to see. The approximate time of deployment is easily visible as the merge commit in the version control system. This time is pretty accurate if you automatically deploy your production branch. If you need a more exact time, you can have your deployment script create a tag on each deployment. This flow prevents the overhead of releasing, tagging, and merging that happens with Git flow.
### Advantages and disadvantages
#### Advantages
- It determines how to perform Continuous Delivery and Continuous Integration
- The git history will be cleaner, less dirty and easier to read
- Ideal when one version is required for production

#### Disadvantages
- It's more complicated than GitHub Flow
- It can be as complex as Git Flow when it needs to support multiple versions in production
