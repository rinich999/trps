﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BarBara
{
    public partial class AddProductForm : Form
    {
        public AddProductForm()
        {
            InitializeComponent();
            this.CenterToScreen();
            NamePBox1.Text = "Введите имя товара";
            NamePBox1.ForeColor = Color.Gray;
            CostPBox1.Text = "Введите цену";
            CostPBox1.ForeColor = Color.Gray;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void NamePBox1_Enter(object sender, EventArgs e)
        {
            if (NamePBox1.Text == "Введите имя товара")
            {
                NamePBox1.Text = "";
                NamePBox1.ForeColor = Color.Black;
            }
        }

        private void NamePBox1_Leave(object sender, EventArgs e)
        {
            if (NamePBox1.Text == "")
            {
                NamePBox1.Text = "Введите имя товара";
                NamePBox1.ForeColor = Color.Gray;
            }
        }

        private void CostPBox1_Enter(object sender, EventArgs e)
        {
            if (CostPBox1.Text == "Введите цену")
            {
                CostPBox1.Text = "";
                CostPBox1.ForeColor = Color.Black;
            }
        }

        private void CostPBox1_Leave(object sender, EventArgs e)
        {
            if (CostPBox1.Text == "")
            {
                CostPBox1.Text = "Введите цену";
                CostPBox1.ForeColor = Color.Gray;

            }
        }
        Point lastPoint;
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void MakeItButton_Click(object sender, EventArgs e)
        {
            if(NamePBox1.Text == "Введіть ім'я товарe")
            {
                MessageBox.Show("Введіть ім'я товару");
                return;
            }
            if (CostPBox1.Text == "Введіть ціну")
            {
                MessageBox.Show("Введіть ціну");
                return;
            }
            if (isProductExists())
            {
                return;
            }
            DataBase1 db = new DataBase1();
            MySqlCommand command = new MySqlCommand("INSERT INTO `products1` (`product_name`, `product_cost`) VALUES(@PN, @PC)", db.getConnection());
            command.Parameters.Add("@PN", MySqlDbType.VarChar).Value = NamePBox1.Text;
            command.Parameters.Add("@PC", MySqlDbType.VarChar).Value = CostPBox1.Text;
            db.openConnection();

            if (command.ExecuteNonQuery() == 1)
            {
                MessageBox.Show("Товар створен");
                Hide();
                this.Close();
            }
            else
                MessageBox.Show("Товар не створен");
            db.closeConnection();
        }

        public Boolean isProductExists()
        {
            DataBase1 db = new DataBase1();

            DataTable table = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter();

            MySqlCommand command = new MySqlCommand("SELECT * FROM `products1` WHERE `product_name` = @pN", db.getConnection());
            command.Parameters.Add("@pN", MySqlDbType.VarChar).Value = NamePBox1.Text;
            adapter.SelectCommand = command;
            adapter.Fill(table);
            db.openConnection();
            if (table.Rows.Count > 0)
            {
                MessageBox.Show("Товар с такою назвою вже існує");
                return true;
            }
            else
            {
                return false;
            }
        }
        

        
    }
}
