﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BarBara
{
    public partial class AdminForm : Form
    {
        public AdminForm()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        Point lastPoint;
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void panel3_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel3_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void ButtonExit_Click(object sender, EventArgs e)
        {
            Hide();
            MainForm1 f = new MainForm1();
            f.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AddProductForm f = new AddProductForm();
            f.ShowDialog();
        }
        int kolVo = 0;
        Point location = new Point(0, 0); // начальная позиция создания кнопок

        private void button1_Click(object sender, EventArgs e)
        {
            DataBase1 db = new DataBase1();

            DataTable table = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter();
            db.openConnection();
            MySqlCommand command = new MySqlCommand("SELECT product_name, product_cost FROM `products1`", db.getConnection());

            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                Button myButton = new Button();// Создан обьект кнопок
                myButton.Name = "myButton" + kolVo + (location.X / 100); // Имя созданой кнопки
                myButton.Text = reader[0].ToString() + " " + reader[1] + " Грн"; // То что написано на кнопке
                myButton.Size = new System.Drawing.Size(100, 100);//Размер созданной кнопки
                myButton.Left = location.Y;// всегда выбирает лево по Х
                myButton.Top = location.X;// всегда выбирает верх по У
                myButton.UseVisualStyleBackColor = true;
                myButton.Click += MyButtons_Click;
                panel2.Controls.Add(myButton);

                if (kolVo == 5) // Система расположения кнопки, сдвиг по вертикале и потом по горизонтали
                {
                    kolVo = 0;
                    location = new Point(location.X + myButton.Width);// спуск кнопки по вертикали
                }
                else
                {
                    kolVo++;
                    location = new Point(location.X, location.Y + myButton.Height);//Тогда сдвиг по горизонтали
                }
            }
            reader.Close();
            db.closeConnection();

        }

        private void MyButtons_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void button2_Click_2(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            DataBase1 db = new DataBase1();

            DataTable table = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter();
            db.openConnection();
            MySqlCommand command = new MySqlCommand("SELECT product_name, product_cost FROM `products1`", db.getConnection());

            /* MySqlCommand command = new MySqlCommand("SELECT * FROM `products1` WHERE `product_name` = @pN AND `product_cost` = @pC", db.getConnection());
             command.Parameters.Add("@pN", MySqlDbType.VarChar).Value = PName;
             command.Parameters.Add("@pC", MySqlDbType.Int32).Value = PCost;
            adapter.SelectCommand = command;
            adapter.Fill(table);    TestTextBox
            
            
           */
            MySqlDataReader reader = command.ExecuteReader();
            int i = 0;
            while (reader.Read())
            {
                listBox1.Items.Insert(i, reader[0].ToString() + "|" + reader[1] + " Грн");
                i++;
            }
            reader.Close();
            db.closeConnection();
        }

       

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            string OSum;
            string selectedProduct;
            int i = listBox1.SelectedIndex;
            if (listBox1.SelectedIndex == -1)
            {
                return;
            }
            selectedProduct = Convert.ToString(listBox1.SelectedItem);
            string[] words = selectedProduct.Split(new char[] { '|' });
            string word1 = words[0];
            string word2 = words[1];
            word2 = word2.Remove(word2.IndexOf(" Грн"));
            // "DELETE FROM `products1` WHERE `products1`.`id_product` = 15"
            DataBase1 db = new DataBase1();
            DataTable table = new DataTable();
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            db.openConnection();
            MySqlCommand command = new MySqlCommand("DELETE FROM `products1` WHERE `products1`.`product_name` = @PN", db.getConnection());
            command.Parameters.Add("@PN", MySqlDbType.VarChar).Value = word1;
            MySqlDataReader reader = command.ExecuteReader();
            reader.Read();
            reader.Close();
            db.closeConnection();
            listBox1.SetSelected(i, true);

            listBox1.Items.RemoveAt(i);
        }
        /*
       private void button3_Click(object sender, EventArgs e)
       {
           RadioButton myRadioButton = new RadioButton();// Создан обьект кнопок
           myRadioButton.Name = "myRadioButton" + kolVo + (location.X / 100); // Имя созданой кнопки
           myRadioButton.Text = "Стол"+ kolVo; // То что написано на кнопке
           myRadioButton.Size = new System.Drawing.Size(100, 100);//Размер созданной кнопки
           myRadioButton.Left = location.Y;// всегда выбирает лево по Х
           myRadioButton.Top = location.X;// всегда выбирает верх по У
           myRadioButton.UseVisualStyleBackColor = true;
           myRadioButton.Click += MyButtons1_Click;
           panel2.Controls.Add(myRadioButton);

           if (kolVo == 5) // Система расположения кнопки, сдвиг по вертикале и потом по горизонтали
           {
               kolVo = 0;
               location = new Point(location.X + myRadioButton.Width);// спуск кнопки по вертикали
           }
           else
           {
               kolVo++;
               location = new Point(location.X, location.Y + myRadioButton.Height);//Тогда сдвиг по горизонтали
           }

       }

       private void MyButtons1_Click(object sender, EventArgs e)
       {
           RadioButton btn = (RadioButton)sender;

       }
       */

    }



}





