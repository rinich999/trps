﻿namespace BarBara
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.CloseButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ButtonSkip = new System.Windows.Forms.Button();
            this.PassCheck1 = new System.Windows.Forms.CheckBox();
            this.PassField1 = new System.Windows.Forms.TextBox();
            this.LoginField1 = new System.Windows.Forms.TextBox();
            this.ButtonLogin1 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            label1.AutoSize = true;
            label1.BackColor = System.Drawing.SystemColors.ControlLight;
            label1.Enabled = false;
            label1.Font = new System.Drawing.Font("Sylfaen", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            label1.Location = new System.Drawing.Point(63, 16);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(249, 52);
            label1.TabIndex = 0;
            label1.Text = "Авторизація";
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.CloseButton);
            this.panel1.Controls.Add(label1);
            this.panel1.Location = new System.Drawing.Point(4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(384, 86);
            this.panel1.TabIndex = 1;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // CloseButton
            // 
            this.CloseButton.BackColor = System.Drawing.SystemColors.Control;
            this.CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseButton.Location = new System.Drawing.Point(354, 3);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(27, 26);
            this.CloseButton.TabIndex = 0;
            this.CloseButton.Text = "X";
            this.CloseButton.UseVisualStyleBackColor = false;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel2.AutoSize = true;
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel2.Controls.Add(this.ButtonSkip);
            this.panel2.Controls.Add(this.PassCheck1);
            this.panel2.Controls.Add(this.PassField1);
            this.panel2.Controls.Add(this.LoginField1);
            this.panel2.Controls.Add(this.ButtonLogin1);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Location = new System.Drawing.Point(4, 97);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(384, 249);
            this.panel2.TabIndex = 2;
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            this.panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            // 
            // ButtonSkip
            // 
            this.ButtonSkip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSkip.Location = new System.Drawing.Point(24, 167);
            this.ButtonSkip.Name = "ButtonSkip";
            this.ButtonSkip.Size = new System.Drawing.Size(154, 48);
            this.ButtonSkip.TabIndex = 9;
            this.ButtonSkip.Text = "Пропустити";
            this.ButtonSkip.UseVisualStyleBackColor = true;
            this.ButtonSkip.Click += new System.EventHandler(this.ButtonSkip_Click);
            // 
            // PassCheck1
            // 
            this.PassCheck1.AutoSize = true;
            this.PassCheck1.Location = new System.Drawing.Point(24, 140);
            this.PassCheck1.Name = "PassCheck1";
            this.PassCheck1.Size = new System.Drawing.Size(144, 21);
            this.PassCheck1.TabIndex = 8;
            this.PassCheck1.Text = "Показати пароль";
            this.PassCheck1.UseVisualStyleBackColor = true;
            this.PassCheck1.CheckedChanged += new System.EventHandler(this.PassCheck1_CheckedChanged);
            // 
            // PassField1
            // 
            this.PassField1.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PassField1.Location = new System.Drawing.Point(72, 83);
            this.PassField1.Multiline = true;
            this.PassField1.Name = "PassField1";
            this.PassField1.Size = new System.Drawing.Size(270, 49);
            this.PassField1.TabIndex = 7;
            // 
            // LoginField1
            // 
            this.LoginField1.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LoginField1.Location = new System.Drawing.Point(72, 16);
            this.LoginField1.Multiline = true;
            this.LoginField1.Name = "LoginField1";
            this.LoginField1.Size = new System.Drawing.Size(271, 50);
            this.LoginField1.TabIndex = 6;
            // 
            // ButtonLogin1
            // 
            this.ButtonLogin1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonLogin1.Location = new System.Drawing.Point(189, 167);
            this.ButtonLogin1.Name = "ButtonLogin1";
            this.ButtonLogin1.Size = new System.Drawing.Size(154, 48);
            this.ButtonLogin1.TabIndex = 4;
            this.ButtonLogin1.Text = "Вхід";
            this.ButtonLogin1.UseVisualStyleBackColor = true;
            this.ButtonLogin1.Click += new System.EventHandler(this.ButtonLogin1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::BarBara.Properties.Resources.lock1;
            this.pictureBox2.Location = new System.Drawing.Point(13, 83);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(53, 50);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::BarBara.Properties.Resources.user1;
            this.pictureBox1.Location = new System.Drawing.Point(13, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(53, 50);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(391, 351);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoginForm";
            this.Text = "Авторизація";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button ButtonLogin1;
        private System.Windows.Forms.TextBox PassField1;
        private System.Windows.Forms.TextBox LoginField1;
        private System.Windows.Forms.CheckBox PassCheck1;
        private System.Windows.Forms.Button ButtonSkip;
    }
}