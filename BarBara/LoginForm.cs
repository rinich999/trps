﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BarBara
{
    public partial class LoginForm : Form
    {

        public LoginForm()
        {
            InitializeComponent();
            PassField1.PasswordChar = '*';
            this.CenterToScreen();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void PassCheck1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox PassCheck1 = (CheckBox)sender; // приводим отправителя к элементу типа CheckBox
            if (PassCheck1.Checked == true)
            {
                PassField1.UseSystemPasswordChar = true;
            }
            else
            {
                PassField1.UseSystemPasswordChar = false;
            }
        }
        
        Point lastPoint;
        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void ButtonLogin1_Click(object sender, EventArgs e)
        {
            
            String LoginUser1 = LoginField1.Text;
            String PassUser1 = PassField1.Text;
            
            DataBase1 db = new DataBase1();

            DataTable table = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter();

            MySqlCommand command = new MySqlCommand("SELECT * FROM `users` WHERE `login_user` = @uL AND `password_user` = @uP", db.getConnection());
            command.Parameters.Add("@uL", MySqlDbType.VarChar).Value = LoginUser1;
            command.Parameters.Add("@uP", MySqlDbType.VarChar).Value = PassUser1;
            adapter.SelectCommand = command;
            adapter.Fill(table);
            db.openConnection();
            if (table.Rows.Count > 0)
            {
                Hide();
                AdminForm f = new AdminForm();
                f.ShowDialog();
                this.Close();

                
            }
            else
            {
                MessageBox.Show("Неправильний логін або пароль");
            }
            db.closeConnection();
        }

        private void ButtonSkip_Click(object sender, EventArgs e)
        {
            Hide();
            MainForm1 f = new MainForm1();
            f.ShowDialog();
            this.Close();
        }
    }
}
