﻿namespace BarBara
{
    partial class AddProductForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddProductForm));
            this.CancelButton = new System.Windows.Forms.Button();
            this.MakeItButton = new System.Windows.Forms.Button();
            this.NamePBox1 = new System.Windows.Forms.TextBox();
            this.CostPBox1 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(238, 144);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(192, 51);
            this.CancelButton.TabIndex = 0;
            this.CancelButton.Text = "Скасувати";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // MakeItButton
            // 
            this.MakeItButton.Location = new System.Drawing.Point(19, 144);
            this.MakeItButton.Name = "MakeItButton";
            this.MakeItButton.Size = new System.Drawing.Size(192, 51);
            this.MakeItButton.TabIndex = 1;
            this.MakeItButton.Text = "Створити";
            this.MakeItButton.UseVisualStyleBackColor = true;
            this.MakeItButton.Click += new System.EventHandler(this.MakeItButton_Click);
            // 
            // NamePBox1
            // 
            this.NamePBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NamePBox1.Location = new System.Drawing.Point(19, 70);
            this.NamePBox1.Multiline = true;
            this.NamePBox1.Name = "NamePBox1";
            this.NamePBox1.Size = new System.Drawing.Size(249, 52);
            this.NamePBox1.TabIndex = 2;
            this.NamePBox1.Enter += new System.EventHandler(this.NamePBox1_Enter);
            this.NamePBox1.Leave += new System.EventHandler(this.NamePBox1_Leave);
            // 
            // CostPBox1
            // 
            this.CostPBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CostPBox1.Location = new System.Drawing.Point(274, 70);
            this.CostPBox1.Multiline = true;
            this.CostPBox1.Name = "CostPBox1";
            this.CostPBox1.Size = new System.Drawing.Size(156, 52);
            this.CostPBox1.TabIndex = 3;
            this.CostPBox1.Enter += new System.EventHandler(this.CostPBox1_Enter);
            this.CostPBox1.Leave += new System.EventHandler(this.CostPBox1_Leave);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.CloseButton);
            this.panel1.Controls.Add(this.NamePBox1);
            this.panel1.Controls.Add(this.CostPBox1);
            this.panel1.Controls.Add(this.MakeItButton);
            this.panel1.Controls.Add(this.CancelButton);
            this.panel1.Location = new System.Drawing.Point(6, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(452, 214);
            this.panel1.TabIndex = 4;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Font = new System.Drawing.Font("Sylfaen", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(43, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(352, 52);
            this.label1.TabIndex = 5;
            this.label1.Text = "Створення товару\r\n";
            // 
            // CloseButton
            // 
            this.CloseButton.BackColor = System.Drawing.SystemColors.Control;
            this.CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseButton.Location = new System.Drawing.Point(423, 0);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(27, 26);
            this.CloseButton.TabIndex = 4;
            this.CloseButton.Text = "X";
            this.CloseButton.UseVisualStyleBackColor = false;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // AddProductForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(465, 224);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddProductForm";
            this.Text = "Створити товар";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button MakeItButton;
        private System.Windows.Forms.TextBox NamePBox1;
        private System.Windows.Forms.TextBox CostPBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label label1;
    }
}