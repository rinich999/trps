﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BarBara
{
    public partial class MainForm1 : Form
    {
        public MainForm1()
        {
            InitializeComponent();
            this.CenterToScreen();

        }

        private void panel3_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel3_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        Point lastPoint;
        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ButtonExit_Click(object sender, EventArgs e)
        {
            Hide();
            LoginForm f = new LoginForm();
            f.ShowDialog();
            this.Close();
        }

        

        


        private void button1_Click(object sender, EventArgs e)
        {

        }
        int kolVo = 0;
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            int intValue = int.Parse(textBox1.Text);
            intValue = 0;
            textBox1.Text = "0";
        }

        private void ButtonAccept_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            int intValue = int.Parse(textBox1.Text);
            intValue = 0;
            textBox1.Text = "0";

        }
        private void MainForm1_Load(object sender, EventArgs e)
        {
            
            Point location = new Point(0, 0);
            DataBase1 db = new DataBase1();

            DataTable table = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter();
            db.openConnection();
            MySqlCommand command = new MySqlCommand("SELECT product_name, product_cost FROM `products1`", db.getConnection());

            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                Button myButton = new Button();// Создан обьект кнопок
                myButton.Name = "myButton" + kolVo + (location.X / 100); // Имя созданой кнопки
                myButton.Text = reader[0].ToString() + " |" + reader[1].ToString() + " Грн|"+ "1 од."; // То что написано на кнопке
                myButton.Size = new System.Drawing.Size(100, 100);//Размер созданной кнопки
                myButton.Left = location.Y;// всегда выбирает лево по Y
                myButton.Top = location.X;// всегда выбирает верх по X
                myButton.UseVisualStyleBackColor = true;
                myButton.Click += MyButtons_Click;
                panel2.Controls.Add(myButton);

                if (kolVo == 5) // Система расположения кнопки, сдвиг по вертикале и потом по горизонтали
                {
                    kolVo = 0;
                    location = new Point(location.X + myButton.Width);// спуск кнопки по вертикали
                }
                else
                {
                    kolVo++;
                    location = new Point(location.X, location.Y + myButton.Height);//Тогда сдвиг по горизонтали
                }
            }
            reader.Close();
            db.closeConnection();

        }

        
        int AmountProduct;
        private void MyButtons_Click(object sender, EventArgs e)
        {
            int intValue = 0;
            Button btn = (Button)sender;
            listBox1.Items.Insert(0, btn.Text);
            int OrderSum;
            int n = listBox1.Items.Count;
            string OSum;
            for (int i = 0; i < 2; i++)
            {
                OSum = Convert.ToString(listBox1.Items[i]);
                string[] words = OSum.Split(new char[] { '|' });
                string word1 = words[0];
                string word2 = words[1];
                string word3 = words[2];
                word2 = word2.Remove(word2.IndexOf(" Грн"));
                word3 = word3.Remove(word3.IndexOf("од"));
                // OSum = Convert.ToString(listBox1.Items[i]);
                // OSum = OSum.Substring(OSum.IndexOf('|') + 1);
                // OSum = OSum.Remove(OSum.IndexOf(" Грн"));
                intValue = intValue + int.Parse(word2)* int.Parse(word3);
                
            }
            textBox1.Text = Convert.ToString(intValue);
            listBox1.SetSelected(0, true);
        }

        
        
        private void increaseButton_Click(object sender, EventArgs e)
        {
            

            string OSum;
            string selectedProduct;
            int intValue = int.Parse(textBox1.Text);
            int i = listBox1.SelectedIndex;
            if(listBox1.SelectedIndex == -1)
            {
                return ;
            }
            selectedProduct = Convert.ToString(listBox1.SelectedItem);
            string[] words = selectedProduct.Split(new char[] { '|' });
            string word1 = words[0];
            string word2 = words[1];
            string word3 = words[2];
            // selectedProduct = selectedProduct.Substring(selectedProduct.LastIndexOf('|') + 1);
            word3 = word3.Remove(word3.IndexOf("од"));
             AmountProduct = int.Parse(word3) + 1 ;
            string allword = word1 + "|" + word2 + "|" + AmountProduct + " од.";
            listBox1.Items.RemoveAt(i);
            listBox1.Items.Insert(i, allword);


            OSum = Convert.ToString(listBox1.Items[i]);
            OSum = OSum.Substring(OSum.IndexOf('|') + 1);
            OSum = OSum.Remove(OSum.IndexOf(" Грн"));
            intValue = int.Parse(OSum) + intValue;
            textBox1.Text = Convert.ToString(intValue);
            listBox1.SetSelected(i, true);
        }
        
       

        private void decreaseButton_Click(object sender, EventArgs e)
        {
            
            
            string OSum;
            string selectedProduct;
            int intValue = int.Parse(textBox1.Text);
            int i = listBox1.SelectedIndex;
            if (listBox1.SelectedIndex == -1)
            {
                return;
            }
            selectedProduct = Convert.ToString(listBox1.SelectedItem);
            string[] words = selectedProduct.Split(new char[] { '|' });
            string word1 = words[0];
            string word2 = words[1];
            string word3 = words[2];
            // selectedProduct = selectedProduct.Substring(selectedProduct.LastIndexOf('|') + 1);
            word3 = word3.Remove(word3.IndexOf("од"));
            if(AmountProduct == 0)
            {
                return;
            }
            AmountProduct = int.Parse(word3) - 1;
            string allword = word1 + "|" + word2 + "|" + AmountProduct + " од.";
            listBox1.Items.RemoveAt(i);
            listBox1.Items.Insert(i, allword);


            OSum = Convert.ToString(listBox1.Items[i]);
            OSum = OSum.Substring(OSum.IndexOf('|') + 1);
            OSum = OSum.Remove(OSum.IndexOf(" Грн"));
            intValue = intValue - int.Parse(OSum) ;
            textBox1.Text = Convert.ToString(intValue);
            listBox1.SetSelected(i, true);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataBase1 db = new DataBase1();

            DataTable table = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter();
            db.openConnection();
            int n = listBox1.Items.Count;
            for (int i = 0; i < n; i++)
            {
                MySqlCommand command3 = new MySqlCommand("INSERT INTO `table1` (`product_is`) VALUES (@PI);", db.getConnection());
                command3.Parameters.Add("@PI", MySqlDbType.VarChar).Value = listBox1.Items[i];
                MySqlDataReader reader3 = command3.ExecuteReader();
                reader3.Read();
                reader3.Close();
                db.closeConnection();
            }
        }

       
         



        private void radioButton1_CheckedChanged_1(object sender, EventArgs e)
        {
            RadioButton radioBtn = (RadioButton)sender;

            string currentButton = radioBtn.Name;
            if (radioBtn.Checked == true)
            {

                listBox1.Items.Clear();
                DataBase1 db = new DataBase1();

                DataTable table = new DataTable();
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                db.openConnection();
                MySqlCommand command = new MySqlCommand("SELECT product_is FROM `table1`", db.getConnection());// когда выбираю стол
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    listBox1.Items.Insert(0, reader[0].ToString());
                }
                reader.Close();
                string OSum;
                int intValue = 0;
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    OSum = Convert.ToString(listBox1.Items[i]);
                    string[] words = OSum.Split(new char[] { '|' });
                    string word1 = words[0];
                    string word2 = words[1];
                    string word3 = words[2];
                    word2 = word2.Remove(word2.IndexOf(" Грн"));
                    word3 = word3.Remove(word3.IndexOf("од"));
                    // OSum = Convert.ToString(listBox1.Items[i]);
                    // OSum = OSum.Substring(OSum.IndexOf('|') + 1);
                    // OSum = OSum.Remove(OSum.IndexOf(" Грн"));
                    intValue = intValue + int.Parse(word2) * int.Parse(word3);

                }
                textBox1.Text = Convert.ToString(intValue);

                db.closeConnection();

            }
            if (radioBtn.Checked == false)
            {
                DataBase1 db = new DataBase1();
                DataTable table = new DataTable();
                db.openConnection();

                MySqlCommand command2 = new MySqlCommand("DELETE FROM `table1`", db.getConnection()); // когда выбираю другой стол и закрываю счет
                MySqlDataReader reader2 = command2.ExecuteReader();
                reader2.Read();
                reader2.Close();
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    MySqlCommand command3 = new MySqlCommand("INSERT INTO `table1` (`product_is`) VALUES (@PI);", db.getConnection());
                    command3.Parameters.Add("@PI", MySqlDbType.VarChar).Value = listBox1.Items[i];
                    MySqlDataReader reader3 = command3.ExecuteReader();
                    reader3.Read();
                    reader3.Close();
                }
                listBox1.Items.Clear();
                int intValue = int.Parse(textBox1.Text);
                intValue = 0;
                textBox1.Text = "0";
                db.closeConnection();

            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioBtn = (RadioButton)sender;

            string currentButton = radioBtn.Name;
            if (radioBtn.Checked == true)
            {

                listBox1.Items.Clear();
                DataBase1 db = new DataBase1();

                DataTable table = new DataTable();
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                db.openConnection();
                MySqlCommand command = new MySqlCommand("SELECT product_is FROM `table2`", db.getConnection());// когда выбираю стол
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    listBox1.Items.Insert(0, reader[0].ToString());
                }
                reader.Close();
                string OSum;
                int intValue = 0;
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    OSum = Convert.ToString(listBox1.Items[i]);
                    string[] words = OSum.Split(new char[] { '|' });
                    string word1 = words[0];
                    string word2 = words[1];
                    string word3 = words[2];
                    word2 = word2.Remove(word2.IndexOf(" Грн"));
                    word3 = word3.Remove(word3.IndexOf("од"));
                    // OSum = Convert.ToString(listBox1.Items[i]);
                    // OSum = OSum.Substring(OSum.IndexOf('|') + 1);
                    // OSum = OSum.Remove(OSum.IndexOf(" Грн"));
                    intValue = intValue + int.Parse(word2) * int.Parse(word3);

                }
                textBox1.Text = Convert.ToString(intValue);

                db.closeConnection();

            }
            if (radioBtn.Checked == false)
            {
                DataBase1 db = new DataBase1();
                DataTable table = new DataTable();
                db.openConnection();

                MySqlCommand command2 = new MySqlCommand("DELETE FROM `table2`", db.getConnection()); // когда выбираю другой стол и закрываю счет
                MySqlDataReader reader2 = command2.ExecuteReader();
                reader2.Read();
                reader2.Close();
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    MySqlCommand command3 = new MySqlCommand("INSERT INTO `table2` (`product_is`) VALUES (@PI);", db.getConnection());
                    command3.Parameters.Add("@PI", MySqlDbType.VarChar).Value = listBox1.Items[i];
                    MySqlDataReader reader3 = command3.ExecuteReader();
                    reader3.Read();
                    reader3.Close();
                }
                listBox1.Items.Clear();
                int intValue = int.Parse(textBox1.Text);
                intValue = 0;
                textBox1.Text = "0";
                db.closeConnection();

            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioBtn = (RadioButton)sender;

            string currentButton = radioBtn.Name;
            if (radioBtn.Checked == true)
            {

                listBox1.Items.Clear();
                DataBase1 db = new DataBase1();

                DataTable table = new DataTable();
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                db.openConnection();
                MySqlCommand command = new MySqlCommand("SELECT product_is FROM `table3`", db.getConnection());// когда выбираю стол
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    listBox1.Items.Insert(0, reader[0].ToString());
                }
                reader.Close();
                string OSum;
                int intValue = 0;
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    OSum = Convert.ToString(listBox1.Items[i]);
                    string[] words = OSum.Split(new char[] { '|' });
                    string word1 = words[0];
                    string word2 = words[1];
                    string word3 = words[2];
                    word2 = word2.Remove(word2.IndexOf(" Грн"));
                    word3 = word3.Remove(word3.IndexOf("од"));
                    // OSum = Convert.ToString(listBox1.Items[i]);
                    // OSum = OSum.Substring(OSum.IndexOf('|') + 1);
                    // OSum = OSum.Remove(OSum.IndexOf(" Грн"));
                    intValue = intValue + int.Parse(word2) * int.Parse(word3);

                }
                textBox1.Text = Convert.ToString(intValue);

                db.closeConnection();

            }
            if (radioBtn.Checked == false)
            {
                DataBase1 db = new DataBase1();
                DataTable table = new DataTable();
                db.openConnection();

                MySqlCommand command2 = new MySqlCommand("DELETE FROM `table3`", db.getConnection()); // когда выбираю другой стол и закрываю счет
                MySqlDataReader reader2 = command2.ExecuteReader();
                reader2.Read();
                reader2.Close();
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    MySqlCommand command3 = new MySqlCommand("INSERT INTO `table3` (`product_is`) VALUES (@PI);", db.getConnection());
                    command3.Parameters.Add("@PI", MySqlDbType.VarChar).Value = listBox1.Items[i];
                    MySqlDataReader reader3 = command3.ExecuteReader();
                    reader3.Read();
                    reader3.Close();
                }
                listBox1.Items.Clear();
                int intValue = int.Parse(textBox1.Text);
                intValue = 0;
                textBox1.Text = "0";
                db.closeConnection();

            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioBtn = (RadioButton)sender;

            string currentButton = radioBtn.Name;
            if (radioBtn.Checked == true)
            {

                listBox1.Items.Clear();
                DataBase1 db = new DataBase1();

                DataTable table = new DataTable();
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                db.openConnection();
                MySqlCommand command = new MySqlCommand("SELECT product_is FROM `table4`", db.getConnection());// когда выбираю стол
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    listBox1.Items.Insert(0, reader[0].ToString());
                }
                reader.Close();
                string OSum;
                int intValue = 0;
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    OSum = Convert.ToString(listBox1.Items[i]);
                    string[] words = OSum.Split(new char[] { '|' });
                    string word1 = words[0];
                    string word2 = words[1];
                    string word3 = words[2];
                    word2 = word2.Remove(word2.IndexOf(" Грн"));
                    word3 = word3.Remove(word3.IndexOf("од"));
                    // OSum = Convert.ToString(listBox1.Items[i]);
                    // OSum = OSum.Substring(OSum.IndexOf('|') + 1);
                    // OSum = OSum.Remove(OSum.IndexOf(" Грн"));
                    intValue = intValue + int.Parse(word2) * int.Parse(word3);

                }
                textBox1.Text = Convert.ToString(intValue);

                db.closeConnection();

            }
            if (radioBtn.Checked == false)
            {
                DataBase1 db = new DataBase1();
                DataTable table = new DataTable();
                db.openConnection();

                MySqlCommand command2 = new MySqlCommand("DELETE FROM `table4`", db.getConnection()); // когда выбираю другой стол и закрываю счет
                MySqlDataReader reader2 = command2.ExecuteReader();
                reader2.Read();
                reader2.Close();
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    MySqlCommand command3 = new MySqlCommand("INSERT INTO `table4` (`product_is`) VALUES (@PI);", db.getConnection());
                    command3.Parameters.Add("@PI", MySqlDbType.VarChar).Value = listBox1.Items[i];
                    MySqlDataReader reader3 = command3.ExecuteReader();
                    reader3.Read();
                    reader3.Close();
                }
                listBox1.Items.Clear();
                int intValue = int.Parse(textBox1.Text);
                intValue = 0;
                textBox1.Text = "0";
                db.closeConnection();

            }
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioBtn = (RadioButton)sender;

            string currentButton = radioBtn.Name;
            if (radioBtn.Checked == true)
            {

                listBox1.Items.Clear();
                DataBase1 db = new DataBase1();

                DataTable table = new DataTable();
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                db.openConnection();
                MySqlCommand command = new MySqlCommand("SELECT product_is FROM `table5`", db.getConnection());// когда выбираю стол
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    listBox1.Items.Insert(0, reader[0].ToString());
                }
                reader.Close();
                string OSum;
                int intValue = 0;
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    OSum = Convert.ToString(listBox1.Items[i]);
                    string[] words = OSum.Split(new char[] { '|' });
                    string word1 = words[0];
                    string word2 = words[1];
                    string word3 = words[2];
                    word2 = word2.Remove(word2.IndexOf(" Грн"));
                    word3 = word3.Remove(word3.IndexOf("од"));
                    // OSum = Convert.ToString(listBox1.Items[i]);
                    // OSum = OSum.Substring(OSum.IndexOf('|') + 1);
                    // OSum = OSum.Remove(OSum.IndexOf(" Грн"));
                    intValue = intValue + int.Parse(word2) * int.Parse(word3);

                }
                textBox1.Text = Convert.ToString(intValue);

                db.closeConnection();

            }
            if (radioBtn.Checked == false)
            {
                DataBase1 db = new DataBase1();
                DataTable table = new DataTable();
                db.openConnection();

                MySqlCommand command2 = new MySqlCommand("DELETE FROM `table5`", db.getConnection()); // когда выбираю другой стол и закрываю счет
                MySqlDataReader reader2 = command2.ExecuteReader();
                reader2.Read();
                reader2.Close();
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    MySqlCommand command3 = new MySqlCommand("INSERT INTO `table5` (`product_is`) VALUES (@PI);", db.getConnection());
                    command3.Parameters.Add("@PI", MySqlDbType.VarChar).Value = listBox1.Items[i];
                    MySqlDataReader reader3 = command3.ExecuteReader();
                    reader3.Read();
                    reader3.Close();
                }
                listBox1.Items.Clear();
                int intValue = int.Parse(textBox1.Text);
                intValue = 0;
                textBox1.Text = "0";
                db.closeConnection();

            }
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioBtn = (RadioButton)sender;

            string currentButton = radioBtn.Name;
            if (radioBtn.Checked == true)
            {

                listBox1.Items.Clear();
                DataBase1 db = new DataBase1();

                DataTable table = new DataTable();
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                db.openConnection();
                MySqlCommand command = new MySqlCommand("SELECT product_is FROM `table6`", db.getConnection());// когда выбираю стол
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    listBox1.Items.Insert(0, reader[0].ToString());
                }
                reader.Close();
                string OSum;
                int intValue = 0;
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    OSum = Convert.ToString(listBox1.Items[i]);
                    string[] words = OSum.Split(new char[] { '|' });
                    string word1 = words[0];
                    string word2 = words[1];
                    string word3 = words[2];
                    word2 = word2.Remove(word2.IndexOf(" Грн"));
                    word3 = word3.Remove(word3.IndexOf("од"));
                    // OSum = Convert.ToString(listBox1.Items[i]);
                    // OSum = OSum.Substring(OSum.IndexOf('|') + 1);
                    // OSum = OSum.Remove(OSum.IndexOf(" Грн"));
                    intValue = intValue + int.Parse(word2) * int.Parse(word3);

                }
                textBox1.Text = Convert.ToString(intValue);

                db.closeConnection();

            }
            if (radioBtn.Checked == false)
            {
                DataBase1 db = new DataBase1();
                DataTable table = new DataTable();
                db.openConnection();

                MySqlCommand command2 = new MySqlCommand("DELETE FROM `table6`", db.getConnection()); // когда выбираю другой стол и закрываю счет
                MySqlDataReader reader2 = command2.ExecuteReader();
                reader2.Read();
                reader2.Close();
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    MySqlCommand command3 = new MySqlCommand("INSERT INTO `table6` (`product_is`) VALUES (@PI);", db.getConnection());
                    command3.Parameters.Add("@PI", MySqlDbType.VarChar).Value = listBox1.Items[i];
                    MySqlDataReader reader3 = command3.ExecuteReader();
                    reader3.Read();
                    reader3.Close();
                }
                listBox1.Items.Clear();
                int intValue = int.Parse(textBox1.Text);
                intValue = 0;
                textBox1.Text = "0";
                db.closeConnection();

            }
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioBtn = (RadioButton)sender;

            string currentButton = radioBtn.Name;
            if (radioBtn.Checked == true)
            {

                listBox1.Items.Clear();
                DataBase1 db = new DataBase1();

                DataTable table = new DataTable();
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                db.openConnection();
                MySqlCommand command = new MySqlCommand("SELECT product_is FROM `table7`", db.getConnection());// когда выбираю стол
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    listBox1.Items.Insert(0, reader[0].ToString());
                }
                reader.Close();
                string OSum;
                int intValue = 0;
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    OSum = Convert.ToString(listBox1.Items[i]);
                    string[] words = OSum.Split(new char[] { '|' });
                    string word1 = words[0];
                    string word2 = words[1];
                    string word3 = words[2];
                    word2 = word2.Remove(word2.IndexOf(" Грн"));
                    word3 = word3.Remove(word3.IndexOf("од"));
                    // OSum = Convert.ToString(listBox1.Items[i]);
                    // OSum = OSum.Substring(OSum.IndexOf('|') + 1);
                    // OSum = OSum.Remove(OSum.IndexOf(" Грн"));
                    intValue = intValue + int.Parse(word2) * int.Parse(word3);

                }
                textBox1.Text = Convert.ToString(intValue);

                db.closeConnection();

            }
            if (radioBtn.Checked == false)
            {
                DataBase1 db = new DataBase1();
                DataTable table = new DataTable();
                db.openConnection();

                MySqlCommand command2 = new MySqlCommand("DELETE FROM `table7`", db.getConnection()); // когда выбираю другой стол и закрываю счет
                MySqlDataReader reader2 = command2.ExecuteReader();
                reader2.Read();
                reader2.Close();
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    MySqlCommand command3 = new MySqlCommand("INSERT INTO `table7` (`product_is`) VALUES (@PI);", db.getConnection());
                    command3.Parameters.Add("@PI", MySqlDbType.VarChar).Value = listBox1.Items[i];
                    MySqlDataReader reader3 = command3.ExecuteReader();
                    reader3.Read();
                    reader3.Close();
                }
                listBox1.Items.Clear();
                int intValue = int.Parse(textBox1.Text);
                intValue = 0;
                textBox1.Text = "0";
                db.closeConnection();

            }
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioBtn = (RadioButton)sender;

            string currentButton = radioBtn.Name;
            if (radioBtn.Checked == true)
            {

                listBox1.Items.Clear();
                DataBase1 db = new DataBase1();

                DataTable table = new DataTable();
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                db.openConnection();
                MySqlCommand command = new MySqlCommand("SELECT product_is FROM `table8`", db.getConnection());// когда выбираю стол
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    listBox1.Items.Insert(0, reader[0].ToString());
                }
                reader.Close();
                string OSum;
                int intValue = 0;
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    OSum = Convert.ToString(listBox1.Items[i]);
                    string[] words = OSum.Split(new char[] { '|' });
                    string word1 = words[0];
                    string word2 = words[1];
                    string word3 = words[2];
                    word2 = word2.Remove(word2.IndexOf(" Грн"));
                    word3 = word3.Remove(word3.IndexOf("од"));
                    // OSum = Convert.ToString(listBox1.Items[i]);
                    // OSum = OSum.Substring(OSum.IndexOf('|') + 1);
                    // OSum = OSum.Remove(OSum.IndexOf(" Грн"));
                    intValue = intValue + int.Parse(word2) * int.Parse(word3);

                }
                textBox1.Text = Convert.ToString(intValue);

                db.closeConnection();

            }
            if (radioBtn.Checked == false)
            {
                DataBase1 db = new DataBase1();
                DataTable table = new DataTable();
                db.openConnection();

                MySqlCommand command2 = new MySqlCommand("DELETE FROM `table8`", db.getConnection()); // когда выбираю другой стол и закрываю счет
                MySqlDataReader reader2 = command2.ExecuteReader();
                reader2.Read();
                reader2.Close();
                int n = listBox1.Items.Count;
                for (int i = 0; i < n; i++)
                {
                    MySqlCommand command3 = new MySqlCommand("INSERT INTO `table8` (`product_is`) VALUES (@PI);", db.getConnection());
                    command3.Parameters.Add("@PI", MySqlDbType.VarChar).Value = listBox1.Items[i];
                    MySqlDataReader reader3 = command3.ExecuteReader();
                    reader3.Read();
                    reader3.Close();
                }
                listBox1.Items.Clear();
                int intValue = int.Parse(textBox1.Text);
                intValue = 0;
                textBox1.Text = "0";
                db.closeConnection();

            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
           
            string OSum;
            string selectedProduct;
            int intValue = int.Parse(textBox1.Text);
            int i = listBox1.SelectedIndex;
            if (listBox1.SelectedIndex == -1)
            {
                return;
            }
            selectedProduct = Convert.ToString(listBox1.SelectedItem);
            string[] words = selectedProduct.Split(new char[] { '|' });
            string word1 = words[0];
            string word2 = words[1];
            string word3 = words[2];
            word2 = word2.Remove(word2.IndexOf(" Грн"));
            word3 = word3.Remove(word3.IndexOf("од"));
            AmountProduct = int.Parse(word3) * int.Parse(word2);

            intValue = intValue - AmountProduct;
            textBox1.Text = Convert.ToString(intValue);
            listBox1.SetSelected(i, true);

            listBox1.Items.RemoveAt(i);


        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
    
}
